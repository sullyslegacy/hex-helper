function doesMatchExist() {
    let matchExist = false;
    const results = [];
    const originalHex = { ...hex };
    
    function rotateAndCheck(rotations) {
        hex = { ...originalHex };
        for(let i = 0; i < rotations; i++) {
            rotateRight();
        }
        let matches = doesVerticalMatchExist(hex);
        if(matches[0] !== 'None') {
            matchExist = true;
        }
        results.push(rotations + " Rotations " + matches);
    }

    function dropAndRotateCheck(rotations) {
        hex = { ...originalHex };
        drop();
        for(let i = 0; i < rotations; i++) {
            rotateRight();
        }
        let matches = doesVerticalMatchExist(hex);
        if(matches[0] !== 'None') {
            results.push(`Drop and Rotate ${rotations} times ` + matches);
        }        
    }

    function rotateAndDropCheck(rotations) {
        hex = { ...originalHex };
        for(let i = 0; i < rotations; i++) {
            rotateRight();
        }
        drop();
        let matches = doesVerticalMatchExist(hex);
        if(matches[0] !== 'None') {
            results.push(rotations + " Rotations and Drop " + matches);
        }        
    }

    function rotateDropAndRotateCheck(rotations1, rotations2) {
        hex = { ...originalHex };
        for(let i = 0; i < rotations1; i++) {
            rotateRight();
        }
        drop();
        for(let i = 0; i < rotations2; i++) {
            rotateRight();
        }
        let matches = doesVerticalMatchExist(hex);
        if(matches[0] !== 'None') {
            results.push(rotations1 + " Rotations and Drop, then Rotate " + rotations2 + " times " + matches);
        }        
    }

    rotateAndCheck(0);
    rotateAndCheck(1);
    rotateAndCheck(2);
    results.push('');

    dropAndRotateCheck(0);
    dropAndRotateCheck(1);
    dropAndRotateCheck(2);
    dropAndRotateCheck(3);
    dropAndRotateCheck(4);
    dropAndRotateCheck(5);
    results.push('');

    rotateAndDropCheck(0);      // Might not doing anything, but prob doesn't hurt
    rotateAndDropCheck(1);
    rotateAndDropCheck(2);
    rotateAndDropCheck(3);
    rotateAndDropCheck(4);
    rotateAndDropCheck(5);
    results.push('');

    const rotDropRot = (rot1) => {
        rotateDropAndRotateCheck(rot1, 1);
        rotateDropAndRotateCheck(rot1, 2);
        rotateDropAndRotateCheck(rot1, 3);
        rotateDropAndRotateCheck(rot1, 4);
        rotateDropAndRotateCheck(rot1, 5);
        results.push('');
    };

    rotDropRot(1);
    rotDropRot(2);
    rotDropRot(3);
    rotDropRot(4);
    rotDropRot(5);
    
    // Output
    hex = { ...originalHex };
    get('output').innerHTML = results.join('</br>');
    return matchExist;
}

function doesVerticalMatchExist(hexConfig) {
    const column1 = [hexConfig['hex3_1'], hexConfig['hex5_1'], hexConfig['hex7_1']];
    const column2 = [hexConfig['hex2_2'], hexConfig['hex4_2'], hexConfig['hex6_2'], hexConfig['hex8_2']];
    const column3 = [hexConfig['hex1_3'], hexConfig['hex3_3'], hexConfig['hex5_3'], hexConfig['hex7_3'], hexConfig['hex9_3']];
    const column4 = [hexConfig['hex2_4'], hexConfig['hex4_4'], hexConfig['hex6_4'], hexConfig['hex8_4']];
    const column5 = [hexConfig['hex3_5'], hexConfig['hex5_5'], hexConfig['hex7_5']];
    
    const columnMatch = (columns, colIndex) => {
        columns = columns.filter(col => col !== 0);
        let last = 0;
        for(var i = 0; i < columns.length; i++) {
            const current = columns[i];
            if(last === current) {
                return `Column ${colIndex} match, value <b>${current}</b>`;
            }
            else {
                last = current;
            }
        }
        return '';
    };

    let matches = [];
    matches.push(columnMatch(column1, '1'));
    matches.push(columnMatch(column2, '2'));
    matches.push(columnMatch(column3, '3'));
    matches.push(columnMatch(column4, '4'));
    matches.push(columnMatch(column5, '5'));
    matches = matches.filter(match => match !== '');
    if(matches.length === 0) {
        matches.push('None');
    }
    return matches;
}

// This actually joins matches
function verticalMatch(hexConfig) {
    const column1 = [hexConfig['hex3_1'], hexConfig['hex5_1'], hexConfig['hex7_1']];
    const column2 = [hexConfig['hex2_2'], hexConfig['hex4_2'], hexConfig['hex6_2'], hexConfig['hex8_2']];
    const column3 = [hexConfig['hex1_3'], hexConfig['hex3_3'], hexConfig['hex5_3'], hexConfig['hex7_3'], hexConfig['hex9_3']];
    const column4 = [hexConfig['hex2_4'], hexConfig['hex4_4'], hexConfig['hex6_4'], hexConfig['hex8_4']];
    const column5 = [hexConfig['hex3_5'], hexConfig['hex5_5'], hexConfig['hex7_5']];

    const columnMatch = (columns, colIndex) => {
        let last = 0;
        let joins = 0;
        let columnOriginal = [...columns];

        const joinHex = (i) => {
            if(joins > 0) {
                let rowIndex = getRowIndex(columnOriginal, i - 1);
                let hexIndex = `hex${rowIndex}_${colIndex}`;
                score = score + (hexConfig[hexIndex] * (joins + 1)) + 1;
                hexConfig[hexIndex] = hexConfig[hexIndex] + joins;
                get('output').innerHTML = get('output').innerHTML + '</br>' + `Joined ${hexConfig[hexIndex]}, Row ${rowIndex}, Col ${colIndex}`;

                // Delete prior joins
                while(joins > 0) {
                    rowIndex = getRowIndex(columnOriginal, i - 1 - joins);
                    hexIndex = `hex${rowIndex}_${colIndex}`;
                    hexConfig[hexIndex] = 0;
                    joins--;
                }
            };
        }

        columns = columns.filter(col => col !== 0);
        for(var i = 0; i < columns.length; i++) {
            const current = columns[i];
            if(last === current) {
                joins++;
            }
            else {
                joinHex(i);
                last = current;
            }
        }
        joinHex(i);
    };

    let matches = [];
    matches.push(columnMatch(column1, '1'));
    matches.push(columnMatch(column2, '2'));
    matches.push(columnMatch(column3, '3'));
    matches.push(columnMatch(column4, '4'));
    matches.push(columnMatch(column5, '5'));
    matches = matches.filter(match => match !== '');
    if(matches.length === 0) {
        matches.push('None');
    }
}

function getRowIndex(columns, i) {
    let amountOfZeros = columns.filter(c => c === 0).length;
    i = i + amountOfZeros;
    if(columns.length === 3) {
        let map = {
            0: 3,
            1: 5,
            2: 7,
        }
        return map[i];
    }
    if(columns.length === 4) {
        let map = {
            0: 2,
            1: 4,
            2: 6,
            3: 8,
        }
        return map[i];
    }
    if(columns.length === 5) {
        let map = {
            0: 1,
            1: 3,
            2: 5,
            3: 7,
            4: 9,
        }
        return map[i];
    }
}

function sortJsonByRows() {
    let keys = Object.keys(hex).sort((a, b) => {
        return a[3] - b[3];
    })
    
    let config = {};
    keys.forEach(key => {
        config[key] = hex[key];
    })
    hex = config;
    display();
    return config;
}

function sortJsonByCols() {
    let keys = Object.keys(hex).sort((a, b) => {
        return a[5] - b[5];
    })
    
    let config = {};
    keys.forEach(key => {
        config[key] = hex[key];
    })
    hex = config;
    display();
    return config;
}