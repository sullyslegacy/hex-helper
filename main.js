let score = 0;
let history = [];

function get(id) {
    return document.getElementById(id);
}

function updateFromJson() {
    hex = JSON.parse(get('json').value);
    display();
}

function display() {
    Object.keys(hex).forEach(hexKey => {
        get(hexKey).innerHTML = `<span class="textSpan">${hex[hexKey]}</span>`;
        
        let colorIndex = get(hexKey).className.indexOf('color');
        if(colorIndex != -1) {
            get(hexKey).className = get(hexKey).className.substring(0, colorIndex);
        }
        get(hexKey).className = get(hexKey).className + ' color' + hex[hexKey];
    });
    get('json').value = JSON.stringify(hex, null, 4);
    get('score').innerHTML = "Score = " + score;
}

/*
 * Event Handler
 */
var keyPressedMap = new Object();
keyPressedMap['enter'] = 13;
keyPressedMap['space'] = 32;
keyPressedMap['left'] = 37;
keyPressedMap['right'] = 39;
keyPressedMap['down'] = 40;


function loaded() {
    display();
    document.addEventListener('keydown', event => {
        // If in textarea don't register events
        if(event.target.id === 'json') {
            return;
        }

        if(event.target.id === 'hexValue' && event.keyCode === keyPressedMap['enter']) {
            updateHexVal();
            return;
        }
        
        if (event.keyCode === keyPressedMap['left']) {
            rotateLeftAndDisplay();
        }
        else if (event.keyCode === keyPressedMap['right']) {
            rotateRightAndDisplay();
        }
        else if (event.keyCode === keyPressedMap['down']) {
            dropAndDisplay();
        }
    }, false) ;
}

/**
 * Wrapper Functions
 */
function rotateLeftAndDisplay() {
    history.push({...hex});
    rotateLeft();
    display();
    doesMatchExist();
}

function rotateRightAndDisplay() {
    history.push({...hex});
    rotateRight();
    display();
    doesMatchExist();
}

function dropAndDisplay() {
    history.push({...hex});
    drop();
    verticalMatch(hex);
    drop();                 // Do another drop in case matches with a space
    createHex();
    if(!openSpotsExist() && !doesMatchExist()) {
        alert("Game Over");
    }
    display();
    doesMatchExist();
}

function undoMove() {
    if (history.length > 0) {
        hex = history.pop();
        display();
    }
    else {
        alert("No more history");
    }
}

/**
 * Logic Functions
 */
function rotateRight() {
    const oldHex = {...hex};
    hex['hex3_5'] = oldHex['hex1_3'];
    hex['hex1_3'] = oldHex['hex3_1'];
    hex['hex3_1'] = oldHex['hex7_1'];
    hex['hex7_1'] = oldHex['hex9_3'];
    hex['hex9_3'] = oldHex['hex7_5'];
    hex['hex7_5'] = oldHex['hex3_5'];

    hex['hex2_4'] = oldHex['hex2_2'];
    hex['hex2_2'] = oldHex['hex5_1'];
    hex['hex5_1'] = oldHex['hex8_2'];
    hex['hex8_2'] = oldHex['hex8_4'];
    hex['hex8_4'] = oldHex['hex5_5'];
    hex['hex5_5'] = oldHex['hex2_4'];
    
    hex['hex4_4'] = oldHex['hex3_3'];
    hex['hex3_3'] = oldHex['hex4_2'];
    hex['hex4_2'] = oldHex['hex6_2'];
    hex['hex6_2'] = oldHex['hex7_3'];
    hex['hex7_3'] = oldHex['hex6_4'];
    hex['hex6_4'] = oldHex['hex4_4'];
}

function rotateLeft() {
    rotateRight();
    rotateRight();
    rotateRight();
    rotateRight();
    rotateRight();
}

function drop() {
    const drops = (rows, column) => {
        rows = rows.sort().reverse();
        for(let i = 1; i < rows.length; i++) {
            let currentRowIndex = `hex${rows[i]}_${column}`; 
            let currentRow = hex[currentRowIndex];
            if(currentRow === 0) {
                continue;
            }

            for(let j = i; j >= 0; j--) {
                let belowRowIndex = `hex${rows[j]}_${column}`; 
                let belowRow = hex[belowRowIndex];
                if(belowRow === 0) {
                    hex[belowRowIndex] = currentRow;
                    hex[currentRowIndex] = 0;
                    //console.log("Drop value " + currentRow + " from " + currentRowIndex + " to " + belowRowIndex);
                    currentRowIndex = belowRowIndex; 
                }
            }
        }
    };

    drops([3, 5, 7], 1);
    drops([3, 5, 7], 5);

    drops([2, 4, 6, 8], 2);
    drops([2, 4, 6, 8], 4);

    drops([1, 3, 5, 7, 9], 3);
}

function createHex() {
    if (!get('createOnDrop').checked) {
        return;
    }

    // Make sure there is an open spot
    if(!openSpotsExist()) {
        console.log("No open spots exist for a new hex");
        return;
    }

    // Find the highest hex, choose a random number up to 6 or this value
    const highHex = Math.max(...Object.values(hex));
    let high = Math.min(highHex, 6);
    let newHex = parseInt(Math.random() * high) + 1;
    if (get('hexNumber').value !== '') {
        newHex = parseInt(get('hexNumber').value);
    }

    // Pick random spot, and add hex
    const spots = openSpots();
    const spotIndex = parseInt(Math.random() * spots.length);
    hex[spots[spotIndex]] = newHex;
}

function openSpots() {
    const spots = [];
    Object.keys(hex).filter(x => {
        if (hex[x] === 0) {
            spots.push(x);
        }
    });
    return spots;
}

function openSpotsExist() {
    const spots = openSpots();
    if(spots.length === 0) {
        return false;
    }
    return true;
}