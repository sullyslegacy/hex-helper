function resetHex() {
    hex = {...hexOg};
    display();
}

function setHexId(e) {
    get('hexId').value = e.id;
    get('hexValue').value = '';
    get('hexValue').focus();
}

function updateHexVal() {
    let hexId = get('hexId').value;
    let hexValue = get('hexValue').value;
    hex[hexId] = parseInt(hexValue);
    display();
}

var hex = {
    "hex1_3": 0,
    "hex2_2": 0,
    "hex2_4": 0,
    "hex3_1": 0,
    "hex3_3": 0,
    "hex3_5": 0,
    "hex4_2": 0,
    "hex4_4": 0,
    "hex5_1": 0,
    "hex5_3": 0,
    "hex5_5": 0,
    "hex6_2": 0,
    "hex6_4": 0,
    "hex7_1": 0,
    "hex7_3": 0,
    "hex7_5": 0,
    "hex8_2": 1,
    "hex8_4": 2,
    "hex9_3": 1
};
var hexOg = {...hex};